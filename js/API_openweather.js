var app = new Vue ({
	el: "#openweather",
	data: {
		humidity_openweather: [],
		loading: false,
      	errored: false
	},
	mounted() {
		axios
			.get (
				"https://api.openweathermap.org/data/2.5/forecast?id=6322752&appid=c1c9bc3307a38be77bdd9dacc4f5b435"
			)
			.then(response => {
				const infograph = response.data.list
				this.humidity_openweather = infograph.map( list => {
					return list.main.humidity;
				});

				this.data_openweather = infograph.map( list => {
					return list.dt_txt;
				});

			// single bar chart
		    var ctx = document.getElementById("openweather_humidity_chart");
		    if (ctx) {
		      ctx.height = 150;
		      var myChart = new Chart(ctx, {
		        type: 'bar',
		        data: {
		          labels: this.data_openweather,
		          datasets: [
		            {
		              label: "Umidade (%)",
		              data: this.humidity_openweather,
		              borderColor: "rgba(0, 123, 255, 0.9)",
		              borderWidth: "0",
		              backgroundColor: "rgba(0, 123, 255, 0.5)"
		            }
		          ]
		        },
		        options: {
		          legend: {
		            position: 'top',
		            labels: {
		              fontFamily: 'Poppins'
		            }

		          },
		          scales: {
		            xAxes: [{
		              ticks: {
		                fontFamily: "Poppins"

		              }
		            }],
		            yAxes: [{
		              ticks: {
		                beginAtZero: true,
		                fontFamily: "Poppins"
		              }
		            }]
		          }
		        }
		      });
		    }

			})
	}
})