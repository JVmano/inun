    var app = new Vue({
      el: "#app",
      data: {
      dates: [],
      probability: [],
      precipitation: [],
      velocity_max: [],
      velocity_min: [],
      velocity_avg: [],
      humidity_min: [],
      humidity_max: [],
      loading: false,
      errored: false
    },
    mounted() {

      axios
        .get (
          "http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/6731/days/15?token=5ffc1cd67c7deb0d259d9388ea9db118"
        )
        .then(response => {
          const infograph = response.data.data
          this.dates = infograph.map(list => {
            return list.date_br;
          });

          this.probability = infograph.map(list => {
            return list.rain.probability;
          });

          this.precipitation = infograph.map(list => {
            return list.rain.precipitation;
          });

          this.velocity_max = infograph.map(list => {
            return list.wind.velocity_max;
          });

          this.velocity_avg = infograph.map(list => {
            return list.wind.velocity_avg;
          });

          this.velocity_min = infograph.map(list => {
            return list.wind.velocity_min;
          });

          this.humidity_min = infograph.map(list => {
            return list.humidity.min;
          });

          this.humidity_max = infograph.map(list => {
            return list.humidity.max;
          });

          /* Gera os gráficos com os dados da API */

            // Humidity chart
            var ctx = document.getElementById("humidity");
            if (ctx) {
              ctx.height = 370;
              var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                  labels: this.dates,
                  type: 'line',
                  defaultFontFamily: 'Poppins',
                  datasets: [{
                    label: "Umidade Mínima",
                    data: this.humidity_min,
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(220,53,69,0.75)',
                    borderWidth: 3,
                    pointStyle: 'circle',
                    pointRadius: 5,
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'rgba(220,53,69,0.75)',
                  }, {
                    label: "Umidade Máxima",
                    data: this.humidity_max,
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(40,167,69,0.75)',
                    borderWidth: 3,
                    pointStyle: 'circle',
                    pointRadius: 5,
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'rgba(40,167,69,0.75)',
                  }]
                },
                options: {
                  responsive: true,
                  tooltips: {
                    mode: 'index',
                    titleFontSize: 12,
                    titleFontColor: '#000',
                    bodyFontColor: '#000',
                    backgroundColor: '#fff',
                    titleFontFamily: 'Poppins',
                    bodyFontFamily: 'Poppins',
                    cornerRadius: 3,
                    intersect: false,
                  },
                  legend: {
                    display: false,
                    labels: {
                      usePointStyle: true,
                      fontFamily: 'Poppins',
                    },
                  },
                  scales: {
                    xAxes: [{
                      display: true,
                      gridLines: {
                        display: false,
                        drawBorder: false
                      },
                      scaleLabel: {
                        display: false,
                        labelString: 'Month'
                      },
                      ticks: {
                        fontFamily: "Poppins"
                      }
                    }],
                    yAxes: [{
                      display: true,
                      gridLines: {
                        display: false,
                        drawBorder: false
                      },
                      scaleLabel: {
                        display: true,
                        labelString: 'Humidade (%)',
                        fontFamily: "Poppins"

                      },
                      ticks: {
                        fontFamily: "Poppins"
                      }
                    }]
                  },
                  title: {
                    display: false,
                    text: 'Normal Legend'
                  }
                }
              });
            }
            // ./Humidity chart

            // Line Chart - Vento
            Chart.defaults.global.defaultFontColor = 'white';
            var ctx = document.getElementById("Wind");
            if (ctx) {
              ctx.height = 370;
              var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                  labels: this.dates,
                  defaultFontFamily: "Poppins",
                  datasets: [
                    {
                      label: "Velocidade Màxima (km/h)",
                      borderColor: "rgba(0,0,0,.09)",
                      borderWidth: "1",
                      backgroundColor: "rgba(2, 180, 232, 0.2)",
                      data: this.velocity_max
                    },
                    {
                      label: "Velocidade Média (km/h)",
                      borderColor: "rgba(0,0,0,.09)",
                      borderWidth: "1",
                      backgroundColor: "rgba(39, 85, 211, 0.3)",
                      pointHighlightStroke: "rgba(26,179,148,1)",
                      data: this.velocity_avg
                    },
                    {
                      label: "Velocidade Mínima (km/h)",
                      borderColor: "rgba(39, 85, 211, 0.3)",
                      borderWidth: "1",
                      backgroundColor: "rgba(72, 0, 191, 0.4)",
                      data: this.velocity_min
                    }
                  ]
                },
                options: {
                  legend: {
                    position: 'top',
                    labels: {
                      fontFamily: 'Poppins'
                    }

                  },
                  responsive: true,
                  tooltips: {
                    mode: 'index',
                    intersect: false
                  },
                  hover: {
                    mode: 'nearest',
                    intersect: true
                  },
                  scales: {
                    xAxes: [{
                      ticks: {
                        fontFamily: "Poppins"

                      }
                    }],
                    yAxes: [{
                      ticks: {
                        beginAtZero: true,
                        fontFamily: "Poppins"
                      }
                    }]
                  }

                }
              });
            }
          // ./Line Chart - Vento

          // Bar Chart - Chuvas
          var ctx = document.getElementById("Rain").getContext('2d');;
          if (ctx) {
            ctx.height = 370;
            var myChart = new Chart(ctx, {
              type: 'bar',
              defaultFontFamily: 'Poppins',
              data: {
                labels: this.dates,
                datasets: [{
                    label: "Chance de Chuva (%)",
                    fill: false,
                    data: this.probability,
                    borderColor: "rgba(0, 184, 233)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 184, 233)",
                    fontFamily: "Poppins"
                  },

                  {
                    label: "Precipitação (mm)",
                    fill: false,
                    data: this.precipitation,
                    borderColor: "rgba(68, 10, 193)",
                    borderWidth: "0",
                    backgroundColor: "rgba(68, 10, 193)",
                    fontFamily: "Poppins"

                  }
                ]
              },
              options: {
                legend: {
                  position: 'top',
                  labels: {
                    fontFamily: 'Poppins'
                  }

                },
                scales: {
                  xAxes: [{
                    ticks: {
                      fontFamily: "Poppins"

                    }
                  }],
                  yAxes: [{
                    ticks: {
                      beginAtZero: true,
                      fontFamily: "Poppins"
                    }
                  }]
                }
              }
            });
          }
        })
        // ./Bar Chart - Chuvas

        .catch(error => {
          console.log(error);
          this.errored = true;
        })
        .finally(() => (this.loading = false));
    }
  })