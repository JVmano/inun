var app = new Vue ({
	el: "#accuweather",
	data: {
		Rain_day: [],
		Rain_night: [],
		PrecipMM_day: [],
		PrecipMM_night: [],
		loading: false,
      	errored: false
	},
	mounted() {
		axios
			.get (
				"http://dataservice.accuweather.com/forecasts/v1/daily/5day/44944?apikey=7aMVGZZQoWgasYE34CIP3rRegW2VCt5R&language=pt-br&details=true&metric=true"
			)
			.then(response => {
				const infograph = response.data.DailyForecasts
				this.Rain_day = infograph.map( list => {
					return list.Day.RainProbability;
				});

				this.Rain_night = infograph.map( list => {
					return list.Night.RainProbability;
				});

				this.dia = infograph.map( list => {
					return list.Date;
				});

				this.PrecipMM_day = infograph.map( list => {
					return list.Day.Rain.Value;
				});

				this.PrecipMM_night = infograph.map( list => {
					return list.Night.Rain.Value;
				});

			// Stacked bar chart
		    var ctx = document.getElementById('Rain_accuweather');
			if (ctx) {
				ctx.height = 150;
				var myChart = new Chart(ctx, {
				  type: 'bar',
				  data: {
				    labels: this.dia,
				    datasets: [
				      {
				        label: 'Chance de Chuva no Dia (%)',
				        data: this.Rain_day, // [67.8]
				        backgroundColor: 'rgba(68, 10, 193)',
				      },
				      {
				        label: 'Chance de Chuva na Noite (%)',
				        data: this.Rain_night, // [20.7]
				        backgroundColor: 'rgba(0, 184, 233)',
				      },
				    ]
				  },
				  options: {
				    scales: {
				      xAxes: [{ stacked: true }],
				      yAxes: [{ stacked: true }]
				    }
				  }
				});
			}

			var ctx = document.getElementById("Precip_Accuweather");
            if (ctx) {
              ctx.height = 150;
              var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                  labels: this.dia,
                  type: 'line',
                  defaultFontFamily: 'Poppins',
                  datasets: [{
                    label: "Precipitação no dia (mm)",
                    data: this.PrecipMM_day,
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(0, 184, 233)',
                    borderWidth: 3,
                    pointStyle: 'circle',
                    pointRadius: 5,
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'rgba(220,53,69,0.75)',
                  }, {
                    label: "Precipitação na noite (mm)",
                    data: this.PrecipMM_night,
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(68, 10, 193)',
                    borderWidth: 3,
                    pointStyle: 'circle',
                    pointRadius: 5,
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'rgba(40,167,69,0.75)',
                  }]
                },
                options: {
                  responsive: true,
                  tooltips: {
                    mode: 'index',
                    titleFontSize: 12,
                    titleFontColor: '#000',
                    bodyFontColor: '#000',
                    backgroundColor: '#fff',
                    titleFontFamily: 'Poppins',
                    bodyFontFamily: 'Poppins',
                    cornerRadius: 3,
                    intersect: false,
                  },
                  legend: {
                    display: false,
                    labels: {
                      usePointStyle: true,
                      fontFamily: 'Poppins',
                    },
                  },
                  scales: {
                    xAxes: [{
                      display: true,
                      gridLines: {
                        display: false,
                        drawBorder: false
                      },
                      scaleLabel: {
                        display: false,
                        labelString: 'Month'
                      },
                      ticks: {
                        fontFamily: "Poppins"
                      }
                    }],
                    yAxes: [{
                      display: true,
                      gridLines: {
                        display: false,
                        drawBorder: false
                      },
                      scaleLabel: {
                        display: true,
                        labelString: 'Precipitação (mm)',
                        fontFamily: "Poppins"

                      },
                      ticks: {
                        fontFamily: "Poppins"
                      }
                    }]
                  },
                  title: {
                    display: false,
                    text: 'Normal Legend'
                  }
                }
              });
            }

			})
	}
})